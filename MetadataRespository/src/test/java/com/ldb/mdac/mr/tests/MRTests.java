/**
 * 
 */
package com.ldb.mdac.mr.tests;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ldb.mdac.mr.datatransfer.DWPackage;
import com.ldb.mdac.mr.datatransfer.Entity;
import com.ldb.mdac.mr.services.MetadataServices;
import com.ldb.mdac.mr.types.AttributeCharacterType;
import com.ldb.mdac.mr.types.AttributeType;
import com.ldb.mdac.mr.types.EntityType;

/**
 * @author Shiv
 *
 */
public class MRTests {

	MetadataServices mrServices;
	Calendar calendar;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		mrServices = new MetadataServices();
		calendar = new GregorianCalendar();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test_01_CreateDWPackage() {

		mrServices.CreateDWPackage("NewPkg", "NewPkg1", "Sample Description", calendar.getTime(), calendar.getTime(),(long) 1);
		
		assertTrue(true);
	}
	
	@Test
	public void test_02_CreateEntity() {
		
		List<DWPackage> savedDWP = mrServices.ReadDWPackage();

		mrServices.CreateEntity("NewEntity", "NewEntity", "Sample Entity", calendar.getTime(), calendar.getTime(),(long) 1, EntityType.DIMESNION, savedDWP.get(0).getId());
		
		assertTrue(true);
	}
	
	@Test
	public void test_03_CreateAttribute() {
		
		List<Entity> savedEntitys = mrServices.ReadEntity();

		mrServices.CreateAttribute("NewAtt", "NewAtt", "Sample Att", calendar.getTime(), calendar.getTime(),(long) 1, AttributeType.SMALL_STRING, AttributeCharacterType.PASS_THROUGH, savedEntitys.get(0).getId());
		
		assertTrue(true);
	}

}
