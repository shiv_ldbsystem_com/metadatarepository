<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/resources/styles/standard.css" />
<title>MDAC</title>
</head>
<body>
	<div id = "wrapper">
		<div id = "header">
		</div>
		<div id = "menu">
			<ul id = "_menu">
 				<span>Attribute</span>
				<li><a href="<%= request.getContextPath() %>/attribute?form">Create Attribute</a></li>
				<li><a href="<%= request.getContextPath() %>/attribute?list">Show Attribute List</a></li>
 				<span>Entity</span>
				<li><a href="<%= request.getContextPath() %>/entity?form">Create Entity</a></li>
				<li><a href="<%= request.getContextPath() %>/entity?list">Show Entity List</a></li>
 				<span>Package</span>
				<li><a href="<%= request.getContextPath() %>/package?form">Create Package</a></li>
				<li><a href="<%= request.getContextPath() %>/package?list">Show Package List</a></li>
			</ul>
		</div>
		<div id = "main">
		</div>
		<div id = "footer">
		</div>
	</div>
</body>
</html>