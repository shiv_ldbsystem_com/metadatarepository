<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/resources/styles/standard.css" />
<title>MDAC</title>
</head>
<body>
	<div id = "wrapper">
		<div id = "header">
		</div>
		<div id = "menu">
			<ul id = "_menu">
 				<li>Attribute</li>
				<li><a href="<%= request.getContextPath() %>/attribute?form">Create Attribute</a></li>
				<li><a href="<%= request.getContextPath() %>/attribute?list">Show Attribute List</a></li>
 				<li>Entity</li>
				<li><a href="<%= request.getContextPath() %>/entity?form">Create Entity</a></li>
				<li><a href="<%= request.getContextPath() %>/entity?list">Show Entity List</a></li>
 				<li>Package</li>
				<li><a href="<%= request.getContextPath() %>/package?form">Create Package</a></li>
				<li><a href="<%= request.getContextPath() %>/package?list">Show Package List</a></li>
			</ul>
		</div>
		<div id = "main">
			<form:form action="/attribute?save">
				<table>
					<tr>
						<td><form:label path="name">Name: </form:label></td>
						<td><form:input path="name" placeholder="Enter Name" /></td>
					</tr>
					<tr>
						<td><form:label path="version">Version: </form:label></td>
						<td><form:input path="version" /></td>
					</tr>
					<tr>
						<td><form:label path="label">Label: </form:label></td>
						<td><form:input path="label" /></td>
					</tr>
					<tr>
						<td><form:label path="description">Description: </form:label></td>
						<td><form:input path="description" /></td>
					</tr>
					<tr>
						<td><form:label path="creationDate">Creation Date: </form:label></td>
						<td><form:input path="creationDate" /></td>
					</tr>
					<tr>
						<td><form:label path="modificationDate">Modification Date: </form:label></td>
						<td><form:input path="modificationDate" /></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" value="Save" /></td>
					</tr>
				</table>
			</form:form>
		</div>
		<div id = "footer">
		</div>
	</div>
</body>
</html>