/**
 * 
 */
package com.ldb.mdac.mr.dataaccess;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ldb.mdac.mr.datatransfer.DWPackage;
import com.ldb.mdac.mr.datatransfer.NamedEntity;
import com.ldb.mdac.mr.hibernate.HibernateUtil;

/**
 * @author Shiv
 *
 */
public class DWPackageImpl implements IDataAccessObject {

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#create(com.ldb.mdac.mr.datatransfer.NamedEntity)
	 */
	public Long create(NamedEntity object) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Long newId = null;
		try {
			transaction = session.beginTransaction();
			DWPackage newPkg = (DWPackage) object;
			newId = (Long) session.save(newPkg);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return newId;
	}

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#update(java.lang.Long, com.ldb.mdac.mr.datatransfer.NamedEntity)
	 */
	public void update(Long id, NamedEntity object) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			DWPackage updPkg = (DWPackage) object;
			session.update(updPkg);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}


	}

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#delete(java.lang.Long)
	 */
	public void delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			DWPackage newDWPkg = (DWPackage) session.get(DWPackage.class, id);
			session.delete(newDWPkg);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

	}

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#read()
	 */
	@SuppressWarnings("unchecked")
	public List<NamedEntity> read() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<NamedEntity> dwPkgList = null;
		try {
			transaction = session.beginTransaction();
			dwPkgList = session.createQuery("from DWPackage").list();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return dwPkgList;
	}

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#readOne(java.lang.Long)
	 */
	public NamedEntity readOne(Long id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		DWPackage savedDWPkg = null;
		try {
			transaction = session.beginTransaction();
			savedDWPkg = (DWPackage) session.get(DWPackage.class, id);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return savedDWPkg;
		
	}
}
