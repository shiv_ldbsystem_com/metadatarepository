/**
 * 
 */
package com.ldb.mdac.mr.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ldb.mdac.mr.datatransfer.Entity;
import com.ldb.mdac.mr.services.MetadataServices;

/**
 * @author Varun
 *
 */
@Controller
@RequestMapping( value="/entity")
public class EntityController {

	MetadataServices service = new MetadataServices();
	
	@RequestMapping( method=RequestMethod.GET)
	public String list(Model uimodel){
		uimodel.addAttribute("message", "Entities");
		return "entity/list";
	}
	
	@RequestMapping( params="form")
	public ModelAndView form( Model uiModel){
		
//		populateEditForm(uimodel, new Object());
		return new ModelAndView("entity/form", "command", new Entity());
	}
	
	@RequestMapping( params="save")
	public void save( Entity form, Model uiModel){
		System.out.println("Name: " + form.getName());
		System.out.println("Label: " + form.getLabel());
	}
	@RequestMapping( params="list")
	public ModelAndView list(){
		System.out.println("listing");
		return new ModelAndView("attributes/list", "command", service.ReadEntity());
	}

	@RequestMapping( params="update")
	public ModelAndView update(){
		Entity form = new Entity();
		form.setName("ATT1");
		form.setLabel("new Attribute!");
		return new ModelAndView("entity/form", "command", form);
	}

}
