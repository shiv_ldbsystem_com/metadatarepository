package com.ldb.mdac.mr.datatransfer;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import com.ldb.mdac.mr.types.*;

@javax.persistence.Entity
public class Entity extends NamedEntity {


	@Enumerated
	@Column(name ="EntityType")
    private EntityType entityType;

    @ManyToOne
    private DWPackage dwpackage;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "entity")
    private Set<Attribute> attributes = new HashSet<Attribute>();
    
	/**
	 * @return the entityType
	 */
	public EntityType getEntityType() {
		return entityType;
	}

	/**
	 * @param entityType the entityType to set
	 */
	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}

	/**
	 * @return the dwpackage
	 */
	public DWPackage getDwpackage() {
		return dwpackage;
	}

	/**
	 * @param dwpackage the dwpackage to set
	 */
	public void setDwpackage(DWPackage dwpackage) {
		this.dwpackage = dwpackage;
	}

	/**
	 * @return the attributes
	 */
	public Set<Attribute> getAttributes() {
		return attributes;
	}

	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(Set<Attribute> attributes) {
		this.attributes = attributes;
	}

}
