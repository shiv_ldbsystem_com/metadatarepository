package com.ldb.mdac.mr.types;


public enum AttributeType {

    SMALL_STRING, TYPE_DATE, LONG_STRING, TYPE_INTEGER;
}
