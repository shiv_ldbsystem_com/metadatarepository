/**
 * 
 */
package com.ldb.mdac.mr.dataaccess;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ldb.mdac.mr.datatransfer.Attribute;
import com.ldb.mdac.mr.datatransfer.NamedEntity;
import com.ldb.mdac.mr.hibernate.HibernateUtil;

/**
 * @author Shiv
 *
 */
public class AttributeImpl implements IDataAccessObject {

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#create(com.ldb.mdac.mr.datatransfer.NamedEntity)
	 */
	public Long create(NamedEntity object) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Long newId = null;
		try {
			transaction = session.beginTransaction();
			Attribute newAttribute = (Attribute) object;
			newId = (Long) session.save(newAttribute);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return newId;
	}

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#update(java.lang.Long, com.ldb.mdac.mr.datatransfer.NamedEntity)
	 */
	public void update(Long id, NamedEntity object) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			Attribute updAttribute = (Attribute) object;
			session.update(updAttribute);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}


	}

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#delete(java.lang.Long)
	 */
	public void delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Attribute delAttribute = (Attribute) session.get(Attribute.class, id);
			session.delete(delAttribute);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

	}

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#read()
	 */
	@SuppressWarnings("unchecked")
	public List<NamedEntity> read() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<NamedEntity> attributeList = null;
		try {
			transaction = session.beginTransaction();
			attributeList = session.createQuery("from Attribute").list();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return attributeList;
	}

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#readOne(java.lang.Long)
	 */
	public NamedEntity readOne(Long id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Attribute savedAttribute = null;
		try {
			transaction = session.beginTransaction();
			savedAttribute = (Attribute) session.get(Attribute.class, id);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return savedAttribute;
		
	}

}
