/**
 * 
 */
package com.ldb.mdac.mr.dataaccess;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.ldb.mdac.mr.datatransfer.Entity;
import com.ldb.mdac.mr.datatransfer.NamedEntity;
import com.ldb.mdac.mr.hibernate.HibernateUtil;

/**
 * @author Shiv
 *
 */
public class EntityImpl implements IDataAccessObject {

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#create(com.ldb.mdac.mr.datatransfer.NamedEntity)
	 */
	public Long create(NamedEntity object) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Long newId = null;
		try {
			transaction = session.beginTransaction();
			Entity newEntity = (Entity) object;
			newId = (Long) session.save(newEntity);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return newId;
	}

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#update(java.lang.Long, com.ldb.mdac.mr.datatransfer.NamedEntity)
	 */
	public void update(Long id, NamedEntity object) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			Entity updEntity = (Entity) object;
			session.update(updEntity);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}


	}

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#delete(java.lang.Long)
	 */
	public void delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Entity delEntity = (Entity) session.get(Entity.class, id);
			session.delete(delEntity);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

	}

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#read()
	 */
	@SuppressWarnings("unchecked")
	public List<NamedEntity> read() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<NamedEntity> entityList = null;
		try {
			transaction = session.beginTransaction();
			entityList = session.createQuery("from Entity").list();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return entityList;
	}

	/* (non-Javadoc)
	 * @see com.ldb.mdac.mr.dataaccess.IDataAccessObject#readOne(java.lang.Long)
	 */
	public NamedEntity readOne(Long id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Entity savedEntity = null;
		try {
			transaction = session.beginTransaction();
			savedEntity = (Entity) session.get(Entity.class, id);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		
		return savedEntity;
		
	}

}
