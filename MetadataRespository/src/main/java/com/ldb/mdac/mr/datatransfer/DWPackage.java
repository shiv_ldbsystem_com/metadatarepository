/**
 * 
 */
package com.ldb.mdac.mr.datatransfer;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;



/**
 * @author Shiv
 *
 */
@javax.persistence.Entity
public class DWPackage  extends NamedEntity{


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "dwpackage")
    private Set<Entity> entities = new HashSet<Entity>();

	/**
	 * @return the entities
	 */
	public Set<Entity> getEntities() {
		return entities;
	}

	/**
	 * @param entities the entities to set
	 */
	public void setEntities(Set<Entity> entities) {
		this.entities = entities;
	}
}
