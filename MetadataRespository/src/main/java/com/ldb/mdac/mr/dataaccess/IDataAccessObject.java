/**
 * 
 */
package com.ldb.mdac.mr.dataaccess;

import java.util.List;

import com.ldb.mdac.mr.datatransfer.NamedEntity;

/**
 * @author Shiv
 *
 */
public interface IDataAccessObject {
	public Long create(NamedEntity object);
	public void update(Long id, NamedEntity object);
	public void delete(Long id);
	public List<NamedEntity> read();
	public NamedEntity readOne(Long id);
}
