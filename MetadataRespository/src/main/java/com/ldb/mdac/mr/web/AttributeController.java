/**
 * @author Varun
 *
 */
package com.ldb.mdac.mr.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ldb.mdac.mr.datatransfer.Attribute;
import com.ldb.mdac.mr.services.MetadataServices;


@Controller
@RequestMapping( value="/attribute")
public class AttributeController {

	MetadataServices service = new MetadataServices();
	
	@RequestMapping( method=RequestMethod.GET)
	public String list(Model uimodel){
		System.out.println("AttributesController:list: I'm hit!");
		uimodel.addAttribute("message", "attributes");
		return "attributes/list";
	}
	
	@RequestMapping( params="form")
	public ModelAndView form( Model uiModel){
		
//		populateEditForm(uimodel, new Object());
		return new ModelAndView("attributes/form", "command", new Attribute());
	}
	
	@RequestMapping( params="save")
	public String save( Attribute attribute, Model uiModel){
		System.out.println("saving..." + attribute.getModificationDate());
		
		service.CreateAttribute( attribute.getName(), attribute.getLabel(), attribute.getDescription(), attribute.getCreationDate(), attribute.getModificationDate(), attribute.getVersion(), attribute.getAttributeType(), attribute.getAttributeCharacterType(), attribute.getId());
        return "redirect:/attributes";
	}
	
	@RequestMapping( params="list")
	public ModelAndView list(){
		System.out.println("listing");
		return new ModelAndView("attributes/list", "command", service.ReadAttribute());
	}
	
	@RequestMapping( params="update")
	public ModelAndView update(Attribute attribute){
		return new ModelAndView("attributes/form", "command", attribute);
	}


/*	private void populateEditForm(Model uimodel, Object object) {
		// TODO Auto-generated method stub
		// Set uimodel to contain an empty or particular attribute object to be rendered on to the form
	}*/
}
