/**
 * 
 */
package com.ldb.mdac.mr.datatransfer;

import javax.persistence.Column;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

import com.ldb.mdac.mr.types.*;

/**
 * @author Shiv
 *
 */
@javax.persistence.Entity
public class Attribute extends NamedEntity {
	
    @Enumerated
    @Column(name ="AttributeCharacterType")
    private AttributeCharacterType attributeCharacterType;

    @Enumerated
    @Column(name ="AttributeType")
    private AttributeType attributeType;

    @ManyToOne
    private Entity entity;

	public AttributeCharacterType getAttributeCharacterType() {
        return this.attributeCharacterType;
    }

	public void setAttributeCharacterType(AttributeCharacterType attributeCharacterType) {
        this.attributeCharacterType = attributeCharacterType;
    }

	public AttributeType getAttributeType() {
        return this.attributeType;
    }

	public void setAttributeType(AttributeType attributeType) {
        this.attributeType = attributeType;
    }

	public Entity getEntity() {
        return this.entity;
    }

	public void setEntity(Entity entity) {
        this.entity = entity;
    }


}
