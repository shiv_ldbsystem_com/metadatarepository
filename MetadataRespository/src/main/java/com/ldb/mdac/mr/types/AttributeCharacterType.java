package com.ldb.mdac.mr.types;


public enum AttributeCharacterType {

    UOM, SCD1, SCD2, PASS_THROUGH;
}
