/**
 * 
 */
package com.ldb.mdac.mr.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ldb.mdac.mr.dataaccess.AttributeImpl;
import com.ldb.mdac.mr.dataaccess.DWPackageImpl;
import com.ldb.mdac.mr.dataaccess.EntityImpl;
import com.ldb.mdac.mr.dataaccess.IDataAccessObject;
import com.ldb.mdac.mr.datatransfer.Attribute;
import com.ldb.mdac.mr.datatransfer.DWPackage;
import com.ldb.mdac.mr.datatransfer.Entity;
import com.ldb.mdac.mr.datatransfer.NamedEntity;
import com.ldb.mdac.mr.types.AttributeCharacterType;
import com.ldb.mdac.mr.types.AttributeType;
import com.ldb.mdac.mr.types.EntityType;

/**
 * @author Shiv
 *
 */
public class MetadataServices {

	public void CreateDWPackage(String name, String label, String description, Date creationDate, Date modificationDate, Long version)
	{
		DWPackage newPkg = new DWPackage();
		newPkg.setName(name);
		newPkg.setLabel(label);
		newPkg.setDescription(description);
		newPkg.setCreationDate(creationDate);
		newPkg.setModificationDate(modificationDate);
		newPkg.setVersion(version);
		
		IDataAccessObject dao = new DWPackageImpl();
		dao.create(newPkg);
	}
	
	public void CreateEntity(String name, String label, String description, Date creationDate, Date modificationDate, Long version, EntityType entityType, Long packageId)
	{
		Entity newEntity = new Entity();
		newEntity.setName(name);
		newEntity.setLabel(label);
		newEntity.setDescription(description);
		newEntity.setCreationDate(creationDate);
		newEntity.setModificationDate(modificationDate);
		newEntity.setVersion(version);
		newEntity.setEntityType(entityType);
		
		IDataAccessObject daoDWp = new DWPackageImpl();
		DWPackage dwpackage = (DWPackage) daoDWp.readOne(packageId);
		
		newEntity.setDwpackage(dwpackage);
		
		IDataAccessObject dao = new EntityImpl();
		dao.create(newEntity);
	}
	
	public void CreateAttribute(String name, String label, String description, Date creationDate, Date modificationDate, Long version,  AttributeType attributeType, AttributeCharacterType attributeCharacterType, Long entityId)
	{
		Attribute newAttribute = new Attribute();
		newAttribute.setName(name);
		newAttribute.setLabel(label);
		newAttribute.setDescription(description);
		newAttribute.setCreationDate(creationDate);
		newAttribute.setModificationDate(modificationDate);
		newAttribute.setVersion(version);
		newAttribute.setAttributeType(attributeType);
		newAttribute.setAttributeCharacterType(attributeCharacterType);

		IDataAccessObject daoEntity = new EntityImpl();
		Entity entity = (Entity) daoEntity.readOne(entityId);
		
		newAttribute.setEntity(entity);
		
		IDataAccessObject dao = new AttributeImpl();
		dao.create(newAttribute);
	}
	
	public void UpdateDWPackage(Long id, String name, String label, String description, Date creationDate, Date modificationDate, Long version)
	{
		DWPackage updPkg = new DWPackage();
		updPkg.setName(name);
		updPkg.setLabel(label);
		updPkg.setDescription(description);
		updPkg.setCreationDate(creationDate);
		updPkg.setModificationDate(modificationDate);
		updPkg.setVersion(version);
		
		IDataAccessObject dao = new DWPackageImpl();
		dao.update(id, updPkg);
	}
	
	public void UpdateEntity(Long id, String name, String label, String description, Date creationDate, Date modificationDate, Long version, EntityType entityType, Long packageId)
	{
		Entity updEntity = new Entity();
		updEntity.setName(name);
		updEntity.setLabel(label);
		updEntity.setDescription(description);
		updEntity.setCreationDate(creationDate);
		updEntity.setModificationDate(modificationDate);
		updEntity.setVersion(version);
		updEntity.setEntityType(entityType);
		
		IDataAccessObject daoDWp = new DWPackageImpl();
		DWPackage dwpackage = (DWPackage) daoDWp.readOne(packageId);
		
		updEntity.setDwpackage(dwpackage);
		
		IDataAccessObject dao = new EntityImpl();
		dao.update(id, updEntity);
	}
	
	public void UpdateAttribute(Long id, String name, String label, String description, Date creationDate, Date modificationDate, Long version,  AttributeType attributeType, AttributeCharacterType attributeCharacterType, Long entityId)
	{
		Attribute updAttribute = new Attribute();
		updAttribute.setName(name);
		updAttribute.setLabel(label);
		updAttribute.setDescription(description);
		updAttribute.setCreationDate(creationDate);
		updAttribute.setModificationDate(modificationDate);
		updAttribute.setVersion(version);
		updAttribute.setAttributeType(attributeType);
		updAttribute.setAttributeCharacterType(attributeCharacterType);

		IDataAccessObject daoEntity = new EntityImpl();
		Entity entity = (Entity) daoEntity.readOne(entityId);
		
		updAttribute.setEntity(entity);
		
		IDataAccessObject dao = new AttributeImpl();
		dao.update(id, updAttribute);
	}
	
	public List<DWPackage> ReadDWPackage()
	{
		IDataAccessObject dao = new DWPackageImpl();
		List<DWPackage> dwPkgList = new ArrayList<DWPackage>();
		for(NamedEntity ne : dao.read())
		{
			DWPackage temp = (DWPackage) ne;
			dwPkgList.add(temp);
		}
		
		return dwPkgList;
	}
	
	public List<Entity> ReadEntity()
	{
		IDataAccessObject dao = new EntityImpl();
		List<Entity> entityList = new ArrayList<Entity>();
		for(NamedEntity ne : dao.read())
		{
			Entity temp = (Entity) ne;
			entityList.add(temp);
		}
		
		return entityList;
		
	}
	
	public List<Attribute> ReadAttribute()
	{
		IDataAccessObject dao = new AttributeImpl();
		List<Attribute> attList = new ArrayList<Attribute>();
		for(NamedEntity ne : dao.read())
		{
			Attribute temp = (Attribute) ne;
			attList.add(temp);
		}
		
		return attList;
	}
	
	
	public void DeleteDWPackage(Long id)
	{
		IDataAccessObject dao = new DWPackageImpl();
		dao.delete(id);
	}
	
	public void DeleteEntity(Long id)
	{
		IDataAccessObject dao = new EntityImpl();
		dao.delete(id);
	}
	
	public void DeleteAttribute(Long id)
	{
		IDataAccessObject dao = new AttributeImpl();
		dao.delete(id);
	}
	
}
