/**
 * 
 */
package com.ldb.mdac.mr.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ldb.mdac.mr.datatransfer.DWPackage;


/**
 * @author Varun
 *
 */
@Controller
@RequestMapping( value="/package")
public class PackageController {

	@RequestMapping( method=RequestMethod.GET)
	public String list(Model uimodel){
		System.out.println("PackageController:list: I'm hit!");
		uimodel.addAttribute("message", "I will retrieve list of all the attributes");
		return "package/list";
	}
	
	@RequestMapping( params="form")
	public ModelAndView form( Model uiModel){
		
//		populateEditForm(uimodel, new Object());
		return new ModelAndView("package/form", "command", new DWPackage());
	}
	
	@RequestMapping( params="save")
	public void save( DWPackage form, Model uiModel){
		System.out.println("Name: " + form.getName());
		System.out.println("Label: " + form.getLabel());
	}
	
	@RequestMapping( params="update")
	public ModelAndView update(){
		DWPackage form = new DWPackage();
		form.setName("ATT1");
		form.setLabel("new Attribute!");
		return new ModelAndView("package/form", "command", form);
	}
}
