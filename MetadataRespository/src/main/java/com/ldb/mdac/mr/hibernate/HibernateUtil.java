package com.ldb.mdac.mr.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class HibernateUtil {

	private static final SessionFactory sessionFactory;

	static {
		try {
			Configuration configuration = new Configuration();
			configuration
					.configure()
					.addPackage("com.ldb.mdac.mr.datatransfer")
					.addAnnotatedClass(
							com.ldb.mdac.mr.datatransfer.NamedEntity.class)
					.addAnnotatedClass(
							com.ldb.mdac.mr.datatransfer.DWPackage.class)
					.addAnnotatedClass(
							com.ldb.mdac.mr.datatransfer.Entity.class)
					.addAnnotatedClass(
							com.ldb.mdac.mr.datatransfer.Attribute.class);

			ServiceRegistry sr = new ServiceRegistryBuilder().applySettings(
					configuration.getProperties()).buildServiceRegistry();

			sessionFactory = configuration.buildSessionFactory(sr);

		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}